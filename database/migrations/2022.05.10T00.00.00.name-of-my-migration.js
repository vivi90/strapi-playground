// path: ./database/migrations/2022.05.10T00.00.00.name-of-my-migration.js

module.exports = {
  async up(knex) {
      // You have full access to the Knex.js API with an already initialized connection to the database

      // EXAMPLE: renaming a column
      await knex.schema.table('categories', table => {
      table.renameColumn('title', 'name');
      });
  },
};
