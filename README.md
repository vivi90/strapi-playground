# strapi-playground
Just testing something.
Might become deleted soon.

## What i have tried
1. Preparing [NodeJS](https://nodejs.org) by using [nvm](https://github.com/nvm-sh/nvm) and installing [Yarn](https://yarnpkg.com):
    ```
    [vrichter@pc strapi-playground]$ nvm install --lts
    Installing latest LTS version.
    v18.13.0 is already installed.
    Now using node v18.13.0 (npm v8.19.3)
    bash: [: -ne: Einstelliger (unärer) Operator erwartet.
    bash: [: -ne: Einstelliger (unärer) Operator erwartet.
    bash: [: -ne: Einstelliger (unärer) Operator erwartet.
    [vrichter@pc strapi-playground]$ node -v
    v18.13.0
    [vrichter@pc strapi-playground]$ npm install -g yarn

    changed 1 package, and audited 2 packages in 806ms

    found 0 vulnerabilities
    ```
2. Installing Strapi (without [TypeScript](https://www.typescriptlang.org)):
    ```
    [vrichter@pc strapi-playground]$ yarn create strapi-app --quickstart .
    yarn create v1.22.19
    [1/4] Resolving packages...
    [2/4] Fetching packages...
    [3/4] Linking dependencies...
    [4/4] Building fresh packages...
    success Installed "create-strapi-app@4.5.5" with binaries:
        - create-strapi-app
    [##########################################################################################################] 106/106Creating a quickstart project.
    Creating a new Strapi application at /home/vrichter/Projekte/strapi-playground.
    Creating files.
    Dependencies installed successfully.

    Your application was created at /home/vrichter/Projekte/strapi-playground.

    Available commands in your project:

    yarn develop
    Start Strapi in watch mode. (Changes in Strapi project files will trigger a server restart)

    yarn start
    Start Strapi without watch mode.

    yarn build
    Build Strapi admin panel.

    yarn strapi
    Display all available commands.

    You can start by doing:

    cd /home/vrichter/Projekte/strapi-playground
    yarn develop

    Running your Strapi application.

    > strapi-playground@0.1.0 develop
    > strapi develop

    Building your admin UI with development configuration...
    Admin UI built successfully
    [2023-01-09 21:53:14.835] info: The Users & Permissions plugin automatically generated a jwt secret and stored it in .env under the name JWT_SECRET.

    Project information                                                          

    ┌────────────────────┬──────────────────────────────────────────────────┐
    │ Time               │ Mon Jan 09 2023 21:53:16 GMT+0100 (Mitteleuropä… │
    │ Launched in        │ 2658 ms                                          │
    │ Environment        │ development                                      │
    │ Process PID        │ 6788                                             │
    │ Version            │ 4.5.5 (node v18.13.0)                            │
    │ Edition            │ Community                                        │
    └────────────────────┴──────────────────────────────────────────────────┘

    Actions available                                                            

    One more thing...
    Create your first administrator 💻 by going to the administration panel at:

    ┌─────────────────────────────┐
    │ http://localhost:1337/admin │
    └─────────────────────────────┘

    [2023-01-09 21:53:16.828] http: GET /admin (26 ms) 200
    [2023-01-09 21:53:17.074] http: GET /admin/project-type (7 ms) 200
    [2023-01-09 21:53:17.079] http: GET /favicon.ico (1 ms) 200
    [2023-01-09 21:53:17.180] http: GET /admin/init (2 ms) 200
    [2023-01-09 21:53:17.304] http: GET /admin/telemetry-properties (2 ms) 401
    ```
3. Creating admin user:
    ![Creating admin user](docs/images/1_create_admin.png)
    ```
    [2023-01-09 21:55:37.224] http: POST /admin/register-admin (124 ms) 200
    [2023-01-09 21:55:37.310] http: GET /admin/information (26 ms) 200
    [2023-01-09 21:55:37.335] http: GET /admin/users/me/permissions (48 ms) 200
    [2023-01-09 21:55:37.349] http: GET /admin/users/me (59 ms) 200
    [2023-01-09 21:55:37.372] http: GET /i18n/locales (18 ms) 200
    [2023-01-09 21:55:37.440] http: GET /content-manager/components (10 ms) 200
    [2023-01-09 21:55:37.455] http: GET /content-manager/content-types (12 ms) 200
    ```
4. Creating `categories` content type:
    ```
    [2023-01-09 21:55:41.862] http: GET /content-type-builder/components (10 ms) 200
    [2023-01-09 21:55:41.880] http: GET /content-type-builder/content-types (15 ms) 200
    [2023-01-09 21:55:41.894] http: GET /content-type-builder/reserved-names (10 ms) 200
    ```
    ![Creating content type](docs/images/2_create_content_type.png)
    ```
    [2023-01-09 21:56:55.801] info: File created: /home/vrichter/Projekte/strapi-playground/src/api/category/controllers/category.js
    [2023-01-09 21:56:55.805] info: File created: /home/vrichter/Projekte/strapi-playground/src/api/category/content-types/category/schema.json
    [2023-01-09 21:56:55.809] info: File created: /home/vrichter/Projekte/strapi-playground/src/api/category/services/category.js
    [2023-01-09 21:56:55.812] info: File changed: /home/vrichter/Projekte/strapi-playground/src/api/category/content-types/category/schema.json
    [2023-01-09 21:56:55.815] info: File created: /home/vrichter/Projekte/strapi-playground/src/api/category/routes/category.js
    [2023-01-09 21:56:56.571] http: POST /content-type-builder/content-types (1167 ms) 201
    The server is restarting


    Project information                                                          

    ┌────────────────────┬──────────────────────────────────────────────────┐
    │ Time               │ Mon Jan 09 2023 21:56:58 GMT+0100 (Mitteleuropä… │
    │ Launched in        │ 761 ms                                           │
    │ Environment        │ development                                      │
    │ Process PID        │ 7171                                             │
    │ Version            │ 4.5.5 (node v18.13.0)                            │
    │ Edition            │ Community                                        │
    └────────────────────┴──────────────────────────────────────────────────┘

    Actions available                                                            

    Welcome back!
    To manage your project 🚀, go to the administration panel at:
    http://localhost:1337/admin

    To access the server ⚡️, go to:
    http://localhost:1337

    [2023-01-09 21:56:59.011] http: HEAD /_health (13 ms) 204
    [2023-01-09 21:56:59.057] http: GET /admin/users/me/permissions (32 ms) 200
    [2023-01-09 21:56:59.089] http: GET /i18n/locales (20 ms) 200
    [2023-01-09 21:56:59.119] http: GET /content-type-builder/components (13 ms) 200
    [2023-01-09 21:56:59.137] http: GET /content-type-builder/content-types (13 ms) 200
    [2023-01-09 21:56:59.151] http: GET /content-type-builder/reserved-names (9 ms) 200
    ```
5. Shut down Strapi
    ```
    ^C
    ```
6. Writing migration file `database/migrations/2022.05.10T00.00.00.name-of-my-migration.js`:
    ```js
    // path: ./database/migrations/2022.05.10T00.00.00.name-of-my-migration.js

    module.exports = {
    async up(knex) {
        // You have full access to the Knex.js API with an already initialized connection to the database

        // EXAMPLE: renaming a column
        await knex.schema.table('categories', table => {
        table.renameColumn('title', 'name');
        });
    },
    };

    ```
7. Testing migration:
    ```
    [vrichter@pc strapi-playground]$ yarn develop
    yarn run v1.22.19
    $ strapi develop
    Building your admin UI with development configuration...
    Admin UI built successfully
    [2023-01-09 22:01:57.653] debug: ⛔️ Server wasn't able to start properly.
    [2023-01-09 22:01:57.654] error: Cannot read properties of undefined (reading 'runMigrations')
    TypeError: Cannot read properties of undefined (reading 'runMigrations')
        at Object.shouldRun (/home/vrichter/Projekte/strapi-playground/node_modules/@strapi/database/lib/migrations/index.js:64:48)
        at async Object.sync (/home/vrichter/Projekte/strapi-playground/node_modules/@strapi/database/lib/schema/index.js:66:11)
        at async Strapi.bootstrap (/home/vrichter/Projekte/strapi-playground/node_modules/@strapi/strapi/lib/Strapi.js:448:5)
        at async Strapi.load (/home/vrichter/Projekte/strapi-playground/node_modules/@strapi/strapi/lib/Strapi.js:478:5)
        at async Strapi.start (/home/vrichter/Projekte/strapi-playground/node_modules/@strapi/strapi/lib/Strapi.js:212:9)
    error Command failed with exit code 1.
    info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
    [vrichter@pc strapi-playground]$ 
    ```
    **Failed!**

## Workaround
According to [issue #15308](https://github.com/strapi/strapi/issues/15308) an workaround might be to replace in `node_modules/@strapi/database/lib/migrations/index.js` the line:
```js
return pending.length > 0 && db.settings.runMigrations;
```
with:
```js
return pending.length > 0 && db.config.settings.runMigrations;
```
The error message disappeared for me but migrations are still not working :(

Read more: [Strapi database migrations documentation](https://docs.strapi.io/developer-docs/latest/developer-resources/database-migrations.html#creating-a-migration-file)

## 🚀 Getting started with Strapi

Strapi comes with a full featured [Command Line Interface](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html) (CLI) which lets you scaffold and manage your project in seconds.

### `develop`

Start your Strapi application with autoReload enabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-develop)

```
npm run develop
# or
yarn develop
```

### `start`

Start your Strapi application with autoReload disabled. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-start)

```
npm run start
# or
yarn start
```

### `build`

Build your admin panel. [Learn more](https://docs.strapi.io/developer-docs/latest/developer-resources/cli/CLI.html#strapi-build)

```
npm run build
# or
yarn build
```

## ⚙️ Deployment

Strapi gives you many possible deployment options for your project. Find the one that suits you on the [deployment section of the documentation](https://docs.strapi.io/developer-docs/latest/setup-deployment-guides/deployment.html).

## 📚 Learn more

- [Resource center](https://strapi.io/resource-center) - Strapi resource center.
- [Strapi documentation](https://docs.strapi.io) - Official Strapi documentation.
- [Strapi tutorials](https://strapi.io/tutorials) - List of tutorials made by the core team and the community.
- [Strapi blog](https://docs.strapi.io) - Official Strapi blog containing articles made by the Strapi team and the community.
- [Changelog](https://strapi.io/changelog) - Find out about the Strapi product updates, new features and general improvements.

Feel free to check out the [Strapi GitHub repository](https://github.com/strapi/strapi). Your feedback and contributions are welcome!

## ✨ Community

- [Discord](https://discord.strapi.io) - Come chat with the Strapi community including the core team.
- [Forum](https://forum.strapi.io/) - Place to discuss, ask questions and find answers, show your Strapi project and get feedback or just talk with other Community members.
- [Awesome Strapi](https://github.com/strapi/awesome-strapi) - A curated list of awesome things related to Strapi.

---

<sub>🤫 Psst! [Strapi is hiring](https://strapi.io/careers).</sub>
